package com.biom4st3r.rpgdamage;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.event.client.ClientSpriteRegistryCallback;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.util.Identifier;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        ClientSpriteRegistryCallback.event(SpriteAtlasTexture.PARTICLE_ATLAS_TEX).register((atlasTexture, registry)->
        {
            for(int i = 0; i <= 9; i++)
            {
                registry.register(new Identifier(ModInit.MODID, "particle/rpg_damage"+i));
            }
            registry.register(new Identifier(ModInit.MODID, "particle/rpg_damage_"));
        });
        
        ClientSidePacketRegistry.INSTANCE.register(new Identifier(ModInit.MODID,"particle_packet"), (context,pbb)->
        {
            float damage = pbb.readFloat();
            double x = pbb.readDouble();
            double y = pbb.readDouble();
            double z = pbb.readDouble();
            double vx = pbb.readDouble();
            double vy = pbb.readDouble();
            double vz = pbb.readDouble();
            context.getTaskQueue().execute(()->
            {
                MinecraftClient.getInstance()
                    .particleManager
                    .addParticle(new RpgDamageParticle(context.getPlayer().world, x, y, z, vx, vy, vz, 1, 1, 1, damage));
            });
        });
    }

    
}