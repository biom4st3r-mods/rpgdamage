package com.biom4st3r.rpgdamage;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.client.particle.ParticleTextureSheet;
import net.minecraft.client.particle.SpriteBillboardParticle;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Quaternion;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

@Environment(EnvType.CLIENT)
public class RpgDamageParticle extends SpriteBillboardParticle {
    float drag = .9f;
    int[] colors = { 0xFF4C69F6, 0xFF4C94FC, 0XFFF6DB35, 0XFFFFC510, 0XFFEE5454 };

    public static float[] intToRpg_F(int i) {
        return new float[] { (((i >> 16 & 0xFF)) / 256.0F), (((i >> 8 & 0xFF)) / 256.0F),
                (((i >> 0 & 0xFF)) / 256.0F) };
    }

    public RpgDamageParticle(World worldIn, double xPos, double yPos, double zPos, double velocityX, double velocityY,
            double velocityZ, float red, float green, float blue, float value) {
        super((ClientWorld) worldIn, xPos, yPos, zPos);
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.velocityZ = velocityZ;
        this.x = xPos;
        this.y = yPos;
        this.z = zPos;
        float[] color = intToRpg_F(colors[worldIn.random.nextInt(colors.length)]);
        this.setColor(color[0], color[1], color[2]);
        this.colorAlpha = 0.5f;

        this.scale = 0.25f;
        this.damage = value;
    }

    public static int maxAge = 40;

    private static Sprite obtainSprite(Identifier id)
    {
        return ((SpriteAtlasTexture)MinecraftClient.getInstance().getTextureManager().getTexture(SpriteAtlasTexture.PARTICLE_ATLAS_TEX)).getSprite(id);
        // ParticleManager
        // return MinecraftClient.getInstance().getSpriteAtlas(SpriteAtlasTexture.PARTICLE_ATLAS_TEX).apply(id);
        // return ((VanillaParticleManager)MinecraftClient.getInstance().particleManager).getAtlas().getSprite(id);
    }

    MeshBuilder builder = RendererAccess.INSTANCE.getRenderer().meshBuilder();
    public void buildGeometry(VertexConsumer vc, Camera camera, float delta) {
        String damageString = String.format("%.1f", damage).replace(".", "_");
        Vec3d vec3d = camera.getPos();
        float x = (float)(MathHelper.lerp(delta, this.prevPosX, this.x) - vec3d.getX());
        float y = (float)(MathHelper.lerp(delta, this.prevPosY, this.y) - vec3d.getY());
        float z = (float)(MathHelper.lerp(delta, this.prevPosZ, this.z) - vec3d.getZ());
        Quaternion quat;
        if (this.angle == 0.0F) {
           quat = camera.getRotation();
        } else {
           quat = new Quaternion(camera.getRotation());
           float i = MathHelper.lerp(delta, this.prevAngle, this.angle);
           quat.hamiltonProduct(Vector3f.POSITIVE_Z.getRadialQuaternion(i));
        }

        float size = this.getSize(delta);
        int index = damageString.length()/-2;
        for (int i = 0; i < damageString.length(); i++)
        {
            Sprite s = obtainSprite(new Identifier(ModInit.MODID, "particle/rpg_damage" + damageString.substring(i, i + 1)));
            Vector3f[] vertexPos = new Vector3f[]{new Vector3f(-1.0F, -1.0F, 0.0F), new Vector3f(-1.0F, 1.0F, 0.0F), new Vector3f(1.0F, 1.0F, 0.0F), new Vector3f(1.0F, -1.0F, 0.0F)};
  
            for(int k = 0; k < 4; ++k) {
                Vector3f vertex = vertexPos[k];
                vertex.add(-index, 0, 0);
                vertex.rotate(quat);
                vertex.scale(size);
                vertex.add(x, y, z);
            }

            float minU = s.getMinU();
            float maxU = s.getMaxU();
            float minV = s.getMinV();
            float maxV = s.getMaxV();
            int p = this.getColorMultiplier(delta);

            vc.vertex(vertexPos[0].getX(), vertexPos[0].getY(), vertexPos[0].getZ()).texture(maxU, maxV).color(this.colorRed, this.colorGreen, this.colorBlue, this.colorAlpha).light(p).next();
            vc.vertex(vertexPos[1].getX(), vertexPos[1].getY(), vertexPos[1].getZ()).texture(maxU, minV).color(this.colorRed, this.colorGreen, this.colorBlue, this.colorAlpha).light(p).next();
            vc.vertex(vertexPos[2].getX(), vertexPos[2].getY(), vertexPos[2].getZ()).texture(minU, minV).color(this.colorRed, this.colorGreen, this.colorBlue, this.colorAlpha).light(p).next();
            vc.vertex(vertexPos[3].getX(), vertexPos[3].getY(), vertexPos[3].getZ()).texture(minU, maxV).color(this.colorRed, this.colorGreen, this.colorBlue, this.colorAlpha).light(p).next();
            index++;
        }
    }

    public double damage = 0.0D;

    public ParticleTextureSheet getType() {
        return ParticleTextureSheet.PARTICLE_SHEET_OPAQUE;
    }

    public void move(double x, double y, double z) {
        this.setBoundingBox(this.getBoundingBox().offset(x, y, z));
        this.repositionFromBoundingBox();
    }

    @Override
    public void tick() {
        if (this.age++ >= RpgDamageParticle.maxAge) {
            this.markDead();
        } else {
            this.prevPosX = this.x;
            this.prevPosY = this.y;
            this.prevPosZ = this.z;
            this.move(this.velocityX, this.velocityY, this.velocityZ);
            this.velocityX *= drag;
            this.velocityY *= drag;
            this.velocityZ *= drag;
            if (this.onGround) {
                this.velocityX *= drag / 2;
                this.velocityZ *= drag / 2;
            }
        }
    }

}